#!/bin/bash
#removing escaping windows character
sed -i -e 's/\r$//' ./bin/*
sed -i -e 's/\r$//' ./config/*

#adding security rights
chmod 777 ./bin/*
chmod 777 ./config/*.groovy

#preparing jar file
rm server.jar
find . -regex '.*[.]jar' -exec sh -c 'x="{}"; cp "$x" "server.jar"' \;
chmod 777 *.jar

./bin/stop.sh

sleep 3

./bin/start.sh