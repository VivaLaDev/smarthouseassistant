#!/bin/bash
#Reading environment variables to this terminal session
set -a; . bin/var.env; set +a;

APP_NAME=${INSTANCE_NAME}.jar

#Preparing working directory path
FILE_PATH=$(realpath $0)
WORK_DIR=${FILE_PATH%/*}
#Changed directory up on 1 level to project root directory
WORK_DIR=${WORK_DIR%/*}

BUILD_DIR=/u/app/build

#Preparing app archive name
APP_ARCHIVE=`echo $1 | cut -d '/' -f 5`
echo "archive name is $APP_ARCHIVE"

#Changing current directory to service directory
cd $WORK_DIR

#Stopping instance
echo -e "\e[00;32m Stopping instance of ${INSTANCE_NAME} service \e[00m"
 . $WORK_DIR/bin/stop.sh
echo -e "\e[00;32m Waiting for stop \e[00m"
sleep 15

#Copying archive to work directory
cp $BUILD_DIR/$APP_ARCHIVE $WORK_DIR

#Unzipping archive
unzip -o $APP_ARCHIVE

#Zipping old console.out
CUR_DATE=$(date +%Y-%m-%d\_%H:%M:%S);
LOG_NAME=console.out-$CUR_DATE.zip
zip ./logs/$LOG_NAME ./logs/console.out
rm ./logs/console.out

#Starting service instance
echo -e "\e[00;32m Starting instance of ${INSTANCE_NAME} \e[00m"
#sudo systemctl start openapi-test1
. $WORK_DIR/bin/start.sh
sleep 40

#Checking startup string from console.out
LOG_STATE=`grep -oE 'Started .+ in .+ seconds \(JVM running for .+\)' ./logs/console.out`

if [[ -n "$LOG_STATE" ]]
    then
        echo -e "\e[00;32mServer started successfully\e[00m"
        rm $APP_ARCHIVE
        rm $BUILD_DIR/$APP_ARCHIVE
    else
       echo -e "\e[00;31mStartup Failed. Check logs/console.out\e[00m"
    fi