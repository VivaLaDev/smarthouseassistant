//
// Built on Fri Aug 17 06:55:37 CEST 2018 by logback-translator
// For more information on configuration files in Groovy
// please see http://logback.qos.ch/manual/groovy.html

// For assistance related to this tool or configuration files
// in general, please contact the logback user mailing list at
//    http://qos.ch/mailman/listinfo/logback-user

// For professional support please see
//   http://www.qos.ch/shop/products/professionalSupport

import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.classic.filter.ThresholdFilter

import static ch.qos.logback.classic.Level.*

def APP_NAME = "assistant"
def PATH_LOG = "logs"
def FILE_PATTERN = "%d{yyyy-MM-dd}_%i.log.gz"
def MAX_FILE_SIZE = "100MB"
def ENCODER_PATTERN = "%date{dd/MM/yy HH:mm:ss} %level [%thread] %logger{10} [%file:%line] %X{GUID} %msg%n"
int MAX_HISTORY = 1

appender("WARN_ERROR_FILE", RollingFileAppender) {
    rollingPolicy(TimeBasedRollingPolicy) {
        fileNamePattern = "${PATH_LOG}/${APP_NAME}-warn-${FILE_PATTERN}"
        timeBasedFileNamingAndTriggeringPolicy(SizeAndTimeBasedFNATP) {
            maxFileSize = MAX_FILE_SIZE
        }
        maxHistory = MAX_HISTORY
    }
    encoder(PatternLayoutEncoder) {
        pattern = "${ENCODER_PATTERN}"
    }
    filter(ThresholdFilter) {
        level = WARN
    }
}
appender("MONGO_DEBUG_FILE", RollingFileAppender) {
    rollingPolicy(TimeBasedRollingPolicy) {
        fileNamePattern = "${PATH_LOG}/${APP_NAME}-mongo-${FILE_PATTERN}"
        timeBasedFileNamingAndTriggeringPolicy(SizeAndTimeBasedFNATP) {
            maxFileSize = MAX_FILE_SIZE
        }
        maxHistory = MAX_HISTORY
    }
    encoder(PatternLayoutEncoder) {
        pattern = "${ENCODER_PATTERN}"
    }
    filter(ThresholdFilter) {
        level = WARN
    }
}
appender("DEBUG_INFO_FILE", RollingFileAppender) {
    rollingPolicy(TimeBasedRollingPolicy) {
        fileNamePattern = "${PATH_LOG}/${APP_NAME}-debug-${FILE_PATTERN}"
        timeBasedFileNamingAndTriggeringPolicy(SizeAndTimeBasedFNATP) {
            maxFileSize = MAX_FILE_SIZE
        }
        maxHistory = MAX_HISTORY
    }
    encoder(PatternLayoutEncoder) {
        pattern = "${ENCODER_PATTERN}"
    }
    filter(ThresholdFilter) {
        level = DEBUG
    }
}
appender("REQUEST_RESPONSE_FILE", RollingFileAppender) {
    rollingPolicy(TimeBasedRollingPolicy) {
        fileNamePattern = "${PATH_LOG}/${APP_NAME}-request-response-${FILE_PATTERN}"
        timeBasedFileNamingAndTriggeringPolicy(SizeAndTimeBasedFNATP) {
            maxFileSize = MAX_FILE_SIZE
        }
        maxHistory = MAX_HISTORY
    }
    encoder(PatternLayoutEncoder) {
        pattern = "${ENCODER_PATTERN}"
    }
    filter(ThresholdFilter) {
        level = DEBUG
    }
}

appender("ALICE_ERROR_MESSAGES", RollingFileAppender) {
    rollingPolicy(TimeBasedRollingPolicy) {
        fileNamePattern = "${PATH_LOG}/${APP_NAME}-alice-error-${FILE_PATTERN}"
        timeBasedFileNamingAndTriggeringPolicy(SizeAndTimeBasedFNATP) {
            maxFileSize = MAX_FILE_SIZE
        }
        maxHistory = MAX_HISTORY
    }
    encoder(PatternLayoutEncoder) {
        pattern = "${ENCODER_PATTERN}"
    }
    filter(ThresholdFilter) {
        level = DEBUG
    }
}

appender("STDOUT", ConsoleAppender) {
    encoder(PatternLayoutEncoder) {
        pattern = "${ENCODER_PATTERN}"
    }
}

logger("com.vivaladev.iotserver.IotAssistantServerApplication", INFO, ["STDOUT", "DEBUG_INFO_FILE"], false)
logger("com.vivaladev.iotserver", DEBUG, ["DEBUG_INFO_FILE", "STDOUT"], false)
logger("com.vivaladev.iotserver.service.android.impl", DEBUG, ["DEBUG_INFO_FILE", "STDOUT"], false)
logger("com.vivaladev.iotserver.dao.service.impl", DEBUG, ["MONGO_DEBUG_FILE"], false)
logger("com.vivaladev.iotserver.config.logging.RequestResponseFilter", DEBUG, ["REQUEST_RESPONSE_FILE"], false)
logger("com.vivaladev.iotserver.config.logging.RequestResponseLoggingInterceptor", DEBUG, ["REQUEST_RESPONSE_FILE"], false)
logger("com.vivaladev.iotserver.exception.RestExceptionHandler", ERROR, ["ALICE_ERROR_MESSAGES"], false)
root(ERROR, ["STDOUT"])