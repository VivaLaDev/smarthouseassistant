package com.vivaladev.iotserver.controller;

import com.vivaladev.iotserver.dao.service.ExternalUserInfoService;
import com.vivaladev.iotserver.service.android.AsyncProcessorService;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.Charset;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(SpringRunner.class)
public class VoiceIntegrationControllerTest {

    private static final String URL_PREFIX_ADI_V_1_VOICE = "/api/v1/voice";
    private static final String URL_INTEGRATION_COMMAND = "/integration/command";
    private MockMvc mockMvc;

    @Mock
    private AsyncProcessorService asyncService;
    @Mock
    private ExternalUserInfoService userInfoService;

    @Before
    public void init() {
        mockMvc = standaloneSetup(new VoiceIntegrationController(asyncService, userInfoService)).build();
    }

    @Test
    public void aliceIntegrationTest() throws Exception {
        mockMvc.perform(
                post(URL_PREFIX_ADI_V_1_VOICE + URL_INTEGRATION_COMMAND)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(IOUtils.toString(getClass().getResourceAsStream("/json/alice/integrationAliceRequest.json"), Charset.defaultCharset())))
                .andExpect(status().isOk());
    }
}
