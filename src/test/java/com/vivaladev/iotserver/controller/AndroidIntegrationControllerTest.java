package com.vivaladev.iotserver.controller;

import com.vivaladev.iotserver.service.command.CommandService;
import com.vivaladev.iotserver.service.user.UserService;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.Charset;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(SpringRunner.class)
public class AndroidIntegrationControllerTest {

    private static final String URL_PREFIX_ADI_V_1_ANDROID = "/api/v1/android";
    private static final String URL_COMMAND = "/command";
    private static final String URL_CONNECTION_TEST_SUCCESS = "/connection/test/success";
    private static final String USER_CODE = "userCode";
    private static final String TEST_DATA_USER_CODE = "12345678";
    private static final String URL_REGISTRATION_NEW_USER = "/user/new";
    private static final String URL_COMMAND_MAPPING = "/command/mapping";
    private static final String URL_COMMAND_MAPPING_DELETE = "/command/mapping/remove";
    private static final String URL_USER_AUTHORIZE = "/user/authorize";
    private static final String URL_WEBHOOK_CHANGE = "/webhook/change";
    private MockMvc mockMvc;

    @Mock
    private CommandService commandService;
    @Mock
    private UserService userService;

    @Before
    public void init() {
        mockMvc = standaloneSetup(new AndroidIntegrationController(commandService, userService)).build();
    }

    @Test
    public void testConnectionTest() throws Exception {
        mockMvc.perform(
                get(URL_PREFIX_ADI_V_1_ANDROID + URL_CONNECTION_TEST_SUCCESS))
                .andExpect(status().isOk());
    }

    @Test
    public void getCommandTest() throws Exception {
        mockMvc.perform(
                post(URL_PREFIX_ADI_V_1_ANDROID + URL_COMMAND)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(IOUtils.toString(getClass().getResourceAsStream("/json/android/userCodeRequest.json"), Charset.defaultCharset())))
                .andExpect(status().isOk());
    }

    @Test
    public void registrationTest() throws Exception {
        mockMvc.perform(
                post(URL_PREFIX_ADI_V_1_ANDROID + URL_REGISTRATION_NEW_USER)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(IOUtils.toString(getClass().getResourceAsStream("/json/android/registrationRequest.json"), Charset.defaultCharset())))
                .andExpect(status().isOk());
    }

    @Test
    public void authorizeUserTest() throws Exception {
        mockMvc.perform(
                post(URL_PREFIX_ADI_V_1_ANDROID + URL_USER_AUTHORIZE)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(IOUtils.toString(getClass().getResourceAsStream("/json/android/authorizationRequest.json"), Charset.defaultCharset())))
                .andExpect(status().isOk());
    }

    @Test
    public void addNewMappingTest() throws Exception {
        mockMvc.perform(
                put(URL_PREFIX_ADI_V_1_ANDROID + URL_COMMAND_MAPPING)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(IOUtils.toString(getClass().getResourceAsStream("/json/android/addNewMapping.json"), Charset.defaultCharset())))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void deleteMapping() throws Exception {
        mockMvc.perform(
                post(URL_PREFIX_ADI_V_1_ANDROID + URL_COMMAND_MAPPING_DELETE)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(IOUtils.toString(getClass().getResourceAsStream("/json/android/deleteCommandMapping.json"), Charset.defaultCharset())))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void changeWebhook() throws Exception{
        mockMvc.perform(
                post(URL_PREFIX_ADI_V_1_ANDROID + URL_WEBHOOK_CHANGE)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(IOUtils.toString(getClass().getResourceAsStream("/json/android/webhookChangeRequest.json"), Charset.defaultCharset())))
                .andExpect(status().isOk());

    }
}
