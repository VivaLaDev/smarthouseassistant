package com.vivaladev.iotserver.service;

import com.vivaladev.iotserver.dao.service.impl.RecognizeServiceImpl;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RecognizeServiceTest {

    private static final String USER_MESSAGE0 = "сделай кек";
    private static final String USER_MESSAGE1 = "сделай кек 15";
    private static final String USER_MESSAGE2 = "сделай кек чебурек";
    private static final String USER_MESSAGE3 = "сделай кек 15 чебурек";

    private static final String EXPECTED_SKILL0 = "кек";
    private static final String EXPECTED_SKILL1 = "кек 15";
    private static final String EXPECTED_SKILL2 = "кек чебурек";
    private static final String EXPECTED_SKILL3 = "кек 15 чебурек";

    private static final String USER_MESSAGE_WITH_CODE0 = "код авторизации 90897867";
    private static final String USER_MESSAGE_WITH_CODE1 = "мой типа код авторизации там вот это вооот 12121212";
    private static final String USER_MESSAGE_WITH_CODE2 = "my code is 23232323";

    private static final String EXPECTED_USER_CODE0 = "90897867";
    private static final String EXPECTED_USER_CODE1 = "12121212";
    private static final String EXPECTED_USER_CODE2 = "23232323";


    @Test
    public void recognizeSkillTest() {
        RecognizeServiceImpl recognizeService = new RecognizeServiceImpl();
        String actualSkill0 = recognizeService.recognizeSkill(USER_MESSAGE0);
        String actualSkill1 = recognizeService.recognizeSkill(USER_MESSAGE1);
        String actualSkill2 = recognizeService.recognizeSkill(USER_MESSAGE2);
        String actualSkill3 = recognizeService.recognizeSkill(USER_MESSAGE3);

        assertEquals(EXPECTED_SKILL0, actualSkill0);
        assertEquals(EXPECTED_SKILL1, actualSkill1);
        assertEquals(EXPECTED_SKILL2, actualSkill2);
        assertEquals(EXPECTED_SKILL3, actualSkill3);
    }

    @Test
    public void recognizeUserCodeTest() {
        RecognizeServiceImpl recognizeService = new RecognizeServiceImpl();
        String actualUserCode0 = recognizeService.recognizeUserCode(USER_MESSAGE_WITH_CODE0);
        String actualUserCode1 = recognizeService.recognizeUserCode(USER_MESSAGE_WITH_CODE1);
        String actualUserCode2 = recognizeService.recognizeUserCode(USER_MESSAGE_WITH_CODE2);

        assertEquals(EXPECTED_USER_CODE0, actualUserCode0);
        assertEquals(EXPECTED_USER_CODE1, actualUserCode1);
        assertEquals(EXPECTED_USER_CODE2, actualUserCode2);
    }
}
