package com.vivaladev.iotserver.controller;

import com.vivaladev.iotserver.dto.android.*;
import com.vivaladev.iotserver.dto.util.InfoDto;
import com.vivaladev.iotserver.service.command.CommandService;
import com.vivaladev.iotserver.service.user.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api("Тестовые адреса для настройки интеграции с андроид клиентом")
@RestController
@RequestMapping("/api/v1/android")
public class AndroidIntegrationController {

    private CommandService commandService;
    private UserService userService;

    public AndroidIntegrationController(CommandService commandService, UserService userService) {
        this.commandService = commandService;
        this.userService = userService;
    }

    @ApiOperation("Проверка доступности ресурса и маппинг ответа")
    @GetMapping("/connection/test/success")
    public InfoDto testConnection() {
        return InfoDto.builder()
                .message("Connect working")
                .build();
    }

    @ApiOperation("Получение списка комманд ")
    @PostMapping("/command")
    public CommandList getCommand(@RequestBody UserCodeRequest userCodeRequest) {
        return commandService.getCommandByUserCode(userCodeRequest.getUserCode());
    }

    @ApiOperation("Регистрация новой команды для пользователя")
    @PutMapping("/command/mapping")
    public InfoDto addNewMapping(@RequestBody @Valid UserCommandMapping userCommandMapping) {
        commandService.addNewSkill2EventMapping(userCommandMapping);
        return InfoDto.builder()
                .message("Command mapping was added")
                .build();
    }

    @ApiOperation("Удаление команды для пользователя")
    @PostMapping("/command/mapping/remove")
    public InfoDto deleteMapping(@RequestBody @Valid UserCommandMapping userCommandMapping) {
        commandService.deleteCommandMapping(userCommandMapping);
        return InfoDto.builder()
                .message("Command mapping was deleted")
                .build();
    }

    @ApiOperation("Регистрация нового пользователя")
    @PostMapping("/user/new")
    public UserCodeResponse registration(@RequestBody @Valid UserRegisterCred registerCred) {
        return userService.registerNewUser(registerCred.getEmail(), registerCred.getPass(), registerCred.getWebhookKey());
    }

    @ApiOperation("Авторизация пользователя")
    @PostMapping("/user/authorize")
    public UserAuthorisationResponse authorizeUser(@RequestBody @Valid UserAuthorizeCred request) {
        return userService.authorizeUser(request.getEmail(), request.getPass());
    }

    @ApiOperation("Изменение вебхука пользователя по userCode")
    @PostMapping("/webhook/change")
    public InfoDto changeWebhook(@RequestBody @Valid WebhookChangeRequest request) {
        userService.changeWebhook(request);
        return InfoDto.builder()
                .message("Webhook was changed")
                .build();
    }
}
