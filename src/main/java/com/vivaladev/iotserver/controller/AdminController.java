package com.vivaladev.iotserver.controller;

import com.vivaladev.iotserver.dto.util.InfoDto;
import com.vivaladev.iotserver.service.internal.InternalService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api("Внутренние проверки работоспособности")
@RequestMapping("/api/vivaladev/internal")
public class AdminController {

    private InternalService internalService;

    public AdminController(InternalService internalService) {
        this.internalService = internalService;
    }

    @ApiOperation("Проверка работы сервиса")
    @GetMapping("/health/check")
    public InfoDto checkHealth(){
        return InfoDto.builder()
                .message("Service is alive")
                .build();
    }

    @ApiOperation("Проверка отправки запросов")
    @GetMapping("/health/self/request")
    public InfoDto selfRequestCheck(){
        return InfoDto.builder()
                .message(internalService.selfRequestCheck())
                .build();
    }
}
