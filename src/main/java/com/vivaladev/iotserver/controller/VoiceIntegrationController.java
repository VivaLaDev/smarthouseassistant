package com.vivaladev.iotserver.controller;

import com.vivaladev.iotserver.dao.service.ExternalUserInfoService;
import com.vivaladev.iotserver.dto.alice.request.AliceRequest;
import com.vivaladev.iotserver.dto.alice.response.AliceResponse;
import com.vivaladev.iotserver.entity.ExternalUserInfo;
import com.vivaladev.iotserver.exception.DefaultAliceResponseProvider;
import com.vivaladev.iotserver.service.android.AsyncProcessorService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api("Интеграция с Алисой")
@RequestMapping("/api/v1/voice")
public class VoiceIntegrationController {

    private AsyncProcessorService asyncService;

    private ExternalUserInfoService userInfoService;

    public VoiceIntegrationController(AsyncProcessorService asyncService,
                                      ExternalUserInfoService userInfoService) {
        this.asyncService = asyncService;
        this.userInfoService = userInfoService;
    }

    @PostMapping("/integration/command")
    public AliceResponse aliceIntegration(@RequestBody AliceRequest request) {
        String clientId = request.getSession().getUserId(); // todo может быть несколько clientId привязанных к 1 userCode, проверить
        if (userInfoService.checkExistingClient(clientId)) {
            //ветка с добавлением команды в очередь, самый позитивный сценарий
            //считается, что пользователь авторизован и его clientId привязан к userCode
            return processCommand(request, clientId);
        } else {
            // ветка с регистрацией клиента, добавления его userCode в базу и ответом о возможности начать работу
            // считается, что userCode содержится в команде вида <код авторизации {userCode}> где userCode подходит под паттерн [0-9]{8}
            boolean authorize = userInfoService.addClientIdWithMappedUserCode(request);
            if (authorize) {
                return DefaultAliceResponseProvider.buildStartWorkingResponse();
            }
        }
        return DefaultAliceResponseProvider.buildUnauthorizeResponse(); // ответить, что необходимо ввести userCode и повторить операцию
    }

    private AliceResponse processCommand(AliceRequest request, String clientId) {
        ExternalUserInfo userInfoByClientId = userInfoService.getUserInfoByClientId(clientId);// получение объекта с userCode
        asyncService.process(request, userInfoByClientId.getUserCode()); //отправка команды в очередь обработки
        return DefaultAliceResponseProvider.buildApprovedCommandResponse(); //стандартный ответ, что команда принята к исполнению
    }
}
