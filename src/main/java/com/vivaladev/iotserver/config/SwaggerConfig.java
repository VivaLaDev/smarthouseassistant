package com.vivaladev.iotserver.config;

import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Configuration
public class SwaggerConfig {

    @Bean
    public Docket swaggerSpringMvcPlugin() {
        final ApiInfo apiInfo = new ApiInfoBuilder()
                .title("IoT assistant v1.0")
                .description("Rest api services for processing operation from android client")
                // Указываем информацию о версии сборки
//                .version(String.format("Version %s (commit %s on branch %s at %s)",
//                        buildProperties.getVersion(), gitProperties.getCommitId(), gitProperties.getBranch(), gitProperties.getCommitTime()))
                .build();
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo)
                .useDefaultResponseMessages(false)
                .select()
                // Убираем из документации встроенные контроллеры спринга
                .paths(Predicates.not(PathSelectors.regex("/actuator/*")))
                .paths(Predicates.not(PathSelectors.regex("/error")))
//                .paths(PathSelectors.any())
                .build();
    }
}
