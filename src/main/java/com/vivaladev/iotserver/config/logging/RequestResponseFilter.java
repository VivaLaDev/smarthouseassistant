package com.vivaladev.iotserver.config.logging;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.AbstractRequestLoggingFilter;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.apache.commons.lang3.CharEncoding.UTF_8;

@Slf4j
public class RequestResponseFilter extends AbstractRequestLoggingFilter {

    private static final Logger logger = LoggerFactory.getLogger(RequestResponseFilter.class);

    private static final String SPLIT_MESSAGE_PARAMETERS_STRING = ";~";
    private static final String INCOMING_REQUEST_TIME = "INCOMING_REQUEST_TIME";
    private static final String JSON_EMPTY_BODY = "{\"body\":\"empty-body\"}";

    public RequestResponseFilter(Integer maxPayload) {
        this.setIncludePayload(true);
        this.setMaxPayloadLength(maxPayload);
    }

    @Override
    protected void beforeRequest(HttpServletRequest request, String message) {
        request.setAttribute(INCOMING_REQUEST_TIME, LocalDateTime.now());
    }

    @Override
    protected void afterRequest(HttpServletRequest request, String message) {
        try {
            Map<String, String> requestParams = new HashMap<>();
            String[] params = message.split(SPLIT_MESSAGE_PARAMETERS_STRING);
            parseRequest(params, requestParams);
        } catch (Exception e) {
            logger.error("Unable to build message. Error is [{}]. Request was [{}] ", e.getMessage(), message);
        }
    }

    private JSONObject buildMetaJsonForLog(Map<String, String> requestParams, HttpServletRequest request) {
        LocalDateTime requestTime = (LocalDateTime) request.getAttribute(INCOMING_REQUEST_TIME);
        Duration duration = Duration.between(requestTime, LocalDateTime.now());
        JSONObject metaInfo = new JSONObject();
        String clientId = request.getHeader("Client-Id");
        metaInfo.put("clientId", clientId);
        metaInfo.put("signature", request.getHeader("Signature"));
        metaInfo.put("contentType", request.getHeader("Content-Type"));
        metaInfo.put("userAgent", request.getHeader("UserCodeResponse-Agent"));
        metaInfo.put("duration", duration.toMillis());
        metaInfo.put("requestTime", requestTime);
        metaInfo.put("requestUri", requestParams.get("uri"));
        metaInfo.put("httpMethod", request.getMethod());
        return metaInfo;
    }

    private String parseJsonToString(JSONObject jsonObject, Map<String, String> requestParams, HttpServletRequest request) {
        JSONObject requestWrapper = new JSONObject();
        requestWrapper.put("metaInfo", jsonObject);

        if (isIncludePayload()) {
            String payload = requestParams.getOrDefault("payload", "{}");
            requestWrapper.put("request", payload);
        } else {
            requestWrapper.put("request", "HIDDEN");
        }

        return requestWrapper.toString();
    }

    private void logResponse(ContentCachingRequestWrapper requestWrapper, ContentCachingResponseWrapper responseWrapper) throws IOException {
        try {
            String message = createMessage(requestWrapper, "", "");
            Map<String, String> requestParams = new HashMap<>();
            String[] params = message.split(SPLIT_MESSAGE_PARAMETERS_STRING);
            parseRequest(params, requestParams);

            JSONObject loggingJson = buildMetaJsonForLog(requestParams, requestWrapper);
            String responseBody =
                    Optional.ofNullable(IOUtils.toString(responseWrapper.getContentInputStream(), UTF_8)).orElse("");
            if (responseBody.isEmpty()) {
                responseBody = JSON_EMPTY_BODY;
            }
            loggingJson.put("responseBody", responseBody.replaceAll("\\\\", ""));
            loggingJson.put("httpStatus", responseWrapper.getStatusCode());
            String requestToLog = parseJsonToString(loggingJson, requestParams, requestWrapper);
            requestToLog = requestToLog.replaceAll("\\\\r\\\\n", "").replaceAll("\\\\", "");
            logger.debug("{} ", requestToLog);
        } catch (Exception e) {
            logger.error("Unable to build response message. Error is [{}].", e);
        } finally {
            responseWrapper.copyBodyToResponse();
        }
    }


    //Extract key and value from params and put it to requestParams map
    private void parseRequest(String[] params, Map<String, String> requestParams) {
        for (String param : params) {
            String[] keyValuePair = param.split("=", 2);
            requestParams.put(keyValuePair[0], keyValuePair[1]);
        }
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        ContentCachingRequestWrapper cachingRequestWrapper = new ContentCachingRequestWrapper(request, getMaxPayloadLength());
        ContentCachingResponseWrapper cachingResponseWrapper = new ContentCachingResponseWrapper(response);

        long requestContentLength = request.getContentLengthLong();
        if (requestContentLength > 5000) {
            logger.warn("Content-Length for {} is {} Can't log payload", request.getRequestURI(), requestContentLength);
        }

        super.doFilterInternal(cachingRequestWrapper, cachingResponseWrapper, filterChain);
        logResponse(cachingRequestWrapper, cachingResponseWrapper);
    }

    @Override
    protected String createMessage(HttpServletRequest request, String prefix, String suffix) {
        StringBuilder msg = new StringBuilder();
        msg.append(prefix);
        msg.append("uri=").append(request.getRequestURI());

        if (isIncludeQueryString()) {
            String queryString = request.getQueryString();
            if (queryString != null) {
                msg.append('?').append(queryString);
            }
        }

        if (isIncludeClientInfo()) {
            String client = request.getRemoteAddr();
            if (StringUtils.hasLength(client)) {
                msg.append(SPLIT_MESSAGE_PARAMETERS_STRING).append("client=").append(client);
            }
            HttpSession session = request.getSession(false);
            if (session != null) {
                msg.append(SPLIT_MESSAGE_PARAMETERS_STRING).append("session=").append(session.getId());
            }
            String user = request.getRemoteUser();
            if (user != null) {
                msg.append(SPLIT_MESSAGE_PARAMETERS_STRING).append("user=").append(user);
            }
        }

        if (isIncludeHeaders()) {
            msg.append(SPLIT_MESSAGE_PARAMETERS_STRING).append("headers=").append(new ServletServerHttpRequest(request).getHeaders());
        }

        if (isIncludePayload()) {
            String payload = getMessagePayload(request);
            if (payload != null) {
                msg.append(SPLIT_MESSAGE_PARAMETERS_STRING).append("payload=").append(payload);
            }
        }

        msg.append(suffix);
        return msg.toString();
    }
}
