package com.vivaladev.iotserver.config.logging;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.UUID;

@Slf4j
public class RequestResponseLoggingInterceptor implements ClientHttpRequestInterceptor {

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        String requestId = UUID.randomUUID().toString();
        logRequest(request, body, requestId);
        ClientHttpResponse response = execution.execute(request, body);
        logResponse(response, requestId);
        return response;
    }

    private void logRequest(HttpRequest request, byte[] body, String requestId) throws IOException {
        if (log.isDebugEnabled()) {
            log.debug("Request {}, URI: {}, Method: {}, Headers: {}, Request body: {}",
                    requestId,
                    request.getURI(),
                    request.getMethod(),
                    request.getHeaders(),
                    new String(body, "UTF-8"));
        }
    }

    private void logResponse(ClientHttpResponse response, String requestId) throws IOException {
        if (log.isDebugEnabled()) {
            log.debug("Response {} Status code: {}, Status text: {}, Headers: {}, Response body: {}",
                    requestId,
                    response.getStatusCode(),
                    response.getStatusText(),
                    response.getHeaders(),
                    StreamUtils.copyToString(response.getBody(), Charset.defaultCharset()));
        }
    }
}
