package com.vivaladev.iotserver.config.async;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
public class AsyncConfig {

    @Bean
    @Qualifier("executorService")
    public ExecutorService executorService() {
        return Executors.newFixedThreadPool(50);
    }
}
