package com.vivaladev.iotserver.config;

import com.vivaladev.iotserver.config.logging.RequestResponseFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;

@Configuration
public class ExternalRequestResponseFilterConfig {

    private static final int MAX_PAYLOAD_LENGTH = 1_000_000;

    @Bean
    public RequestResponseFilter requestLoggingFilter() {
        RequestResponseFilter filter = new RequestResponseFilter(MAX_PAYLOAD_LENGTH);
        filter.setIncludeQueryString(true);
        filter.setIncludePayload(true);
        filter.setAfterMessagePrefix("");
        filter.setBeforeMessagePrefix("");
        filter.setAfterMessageSuffix("");
        filter.setBeforeMessageSuffix("");
        filter.setMaxPayloadLength(MAX_PAYLOAD_LENGTH);
        return filter;
    }

    @Bean
    public FilterRegistrationBean<RequestResponseFilter> filterRegistration(RequestResponseFilter filter) {
        FilterRegistrationBean<RequestResponseFilter> filterRegistration = new FilterRegistrationBean<>();
        filterRegistration.setFilter(filter);
        filterRegistration.addUrlPatterns("/api/*");
        filterRegistration.setOrder(Ordered.LOWEST_PRECEDENCE - 2);
        return filterRegistration;
    }
}
