package com.vivaladev.iotserver.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;

@Data
@Document(collection = "externalUserMatching")
public class ExternalUserInfo {

    @Id
    private String clientId;

    @NotNull
    private String userCode;
}
