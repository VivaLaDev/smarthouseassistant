package com.vivaladev.iotserver.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;

@Data
@Document(collection = "userCommandRecognize")
public class UserCommandRecognize {

    @Id
    private String userCode;

    private Map<String, String> skill2EventMap;
}
