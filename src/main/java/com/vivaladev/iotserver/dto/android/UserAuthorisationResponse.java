package com.vivaladev.iotserver.dto.android;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserAuthorisationResponse {

    private String userCode;

    private String userWebhookKey;
}
