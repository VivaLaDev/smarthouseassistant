package com.vivaladev.iotserver.dto.android;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class UserCommandMapping {

    @NotBlank
    private String skillName;

    @NotBlank
    private String eventName;

    @NotBlank
    private String userCode;
}
