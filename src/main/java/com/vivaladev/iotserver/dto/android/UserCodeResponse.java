package com.vivaladev.iotserver.dto.android;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserCodeResponse {

    private String userCode;
}
