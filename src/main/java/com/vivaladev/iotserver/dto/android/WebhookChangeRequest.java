package com.vivaladev.iotserver.dto.android;

import lombok.Data;

@Data
public class WebhookChangeRequest {

    private String userCode;

    private String webhookKey;
}
