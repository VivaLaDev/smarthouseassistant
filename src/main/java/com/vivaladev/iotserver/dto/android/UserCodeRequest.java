package com.vivaladev.iotserver.dto.android;

import lombok.Data;

@Data
public class UserCodeRequest {

    private String userCode;
}
