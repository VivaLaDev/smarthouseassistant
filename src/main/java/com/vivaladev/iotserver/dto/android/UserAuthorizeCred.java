package com.vivaladev.iotserver.dto.android;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class UserAuthorizeCred {

    @NotBlank
    private String email;

    @NotBlank
    private String pass;
}
