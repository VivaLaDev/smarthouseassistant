package com.vivaladev.iotserver.dto.android;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class UserRegisterCred {

    @NotBlank
    private String email;

    @NotBlank
    private String pass;

    @NotBlank
    private String webhookKey;
}
