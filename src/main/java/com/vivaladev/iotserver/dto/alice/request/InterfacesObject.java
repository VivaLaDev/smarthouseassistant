package com.vivaladev.iotserver.dto.alice.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("Интерфейсы, доступные на устройстве пользователя.")
public class InterfacesObject {

    @ApiModelProperty(notes = "Пользователь может видеть ответ навыка на экране и открывать ссылки в браузере.")
    private Object screen; // TODO: 21.04.2019 change type
}
