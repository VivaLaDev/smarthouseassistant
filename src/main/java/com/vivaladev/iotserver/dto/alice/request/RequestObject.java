package com.vivaladev.iotserver.dto.alice.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.json.JSONObject;

@Data
@ApiModel(description = "Данные, полученные от пользователя.")
public class RequestObject {

    @ApiModelProperty(notes = "Запрос, который был передан вместе с командой активации навыка.\n Например, если пользователь активирует навык словами «спроси у Сбербанка где ближайшее отделение», в этом поле будет передана строка «где ближайшее отделение».")
    private String command;

    @JsonProperty("original_utterance")
    @ApiModelProperty(notes = "Полный текст пользовательского запроса, максимум 1024 символа.")
    private String originalUtterance;

    @ApiModelProperty(notes = "Тип ввода, обязательное свойство.")
    private String type;

    @ApiModelProperty(notes = "Формальные характеристики реплики, которые удалось выделить Яндекс.Диалогам. Отсутствует, если ни одно из вложенных свойств не применимо.")
    private MarkupObject markup;

    @ApiModelProperty(notes = "JSON, полученный с нажатой кнопкой от обработчика навыка (в ответе на предыдущий запрос), максимум 4096 байт.")
    private JSONObject payload;

    @ApiModelProperty(notes = "Слова и именованные сущности, которые Диалоги извлекли из запроса пользователя. Подробнее https://tech.yandex.ru/dialogs/alice/doc/nlu-docpage/")
    private NluObject nlu;
}
