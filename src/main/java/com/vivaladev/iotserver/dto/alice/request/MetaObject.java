package com.vivaladev.iotserver.dto.alice.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "Информация об устройстве, с помощью которого пользователь разговаривает с Алисой.")
public class MetaObject {

    @ApiModelProperty(notes = "Язык в POSIX-формате, максимум 64 символа.")
    private String locale;

    @ApiModelProperty(notes = "Название часового пояса, включая алиасы, максимум 64 символа.")
    private String timezone;

    @ApiModelProperty(notes = "Идентификатор устройства и приложения, в котором идет разговор, максимум 1024 символа.")
    @JsonProperty("client_id")
    private String clientId;

    @ApiModelProperty(notes = "Интерфейсы, доступные на устройстве пользователя.")
    private InterfacesObject interfaces;
}
