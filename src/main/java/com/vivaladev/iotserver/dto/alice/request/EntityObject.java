package com.vivaladev.iotserver.dto.alice.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(description = "Именованная сущность.")
public class EntityObject {

    @ApiModelProperty(notes = "Обозначение начала и конца именованной сущности в массиве слов. Нумерация слов в массиве начинается с 0.")
    private TokenObject tokens;

    @ApiModelProperty(notes = "Тип именованной сущности. Возможные значения:\nYANDEX.DATETIME — дата и время, абсолютные или относительные.\nYANDEX.FIO — фамилия, имя и отчество.\nYANDEX.GEO — местоположение (адрес или аэропорт).\nYANDEX.NUMBER — число, целое или с плавающей точкой.")
    private String type;

    @ApiModelProperty(notes = "Значение. Может быть как сложного типа, так и числом, целым или дробным. В настоящее время поддерживаются только числа")
    private Object value;

//    private EntityValuesObject value; // TODO: 21.04.2019 решить что делать со сложными запросами
}
