package com.vivaladev.iotserver.dto.alice.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "Обозначение начала и конца именованной сущности в массиве слов. Нумерация слов в массиве начинается с 0.")
public class TokenObject {

    @ApiModelProperty(notes = "Первое слово именованной сущности.")
    private Integer start;

    @ApiModelProperty(notes = "Первое слово после именованной сущности.")
    private Integer end;
}
