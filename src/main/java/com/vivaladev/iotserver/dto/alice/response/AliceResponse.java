package com.vivaladev.iotserver.dto.alice.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@ApiModel(description = "Ответ для Алисы")
public class AliceResponse {

    @Valid
    @NotNull
    @JsonProperty(required = true)
    @ApiModelProperty(notes = "Контейнер для объектов ответа Алисы")
    private ResponseObject response;

    @Valid
    @NotNull
    @JsonProperty(required = true)
    @ApiModelProperty(notes = "Объект сессии пользователя")
    private SessionObjectResponse session;

    @NotBlank
    @JsonProperty(required = true)
    @ApiModelProperty(notes = "Версия протокола, всегда 1.0")
    private String version;
}
