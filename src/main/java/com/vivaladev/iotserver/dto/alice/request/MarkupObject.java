package com.vivaladev.iotserver.dto.alice.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("Формальные характеристики реплики, которые удалось выделить Яндекс.Диалогам. Отсутствует, если ни одно из вложенных свойств не применимо.")
public class MarkupObject {

    @JsonProperty("dangerous_context")
    @ApiModelProperty(notes = "Признак реплики, которая содержит криминальный подтекст (самоубийство, разжигание ненависти, угрозы).\n Вы можете настроить навык на определенную реакцию для таких случаев — например, отвечать «Не понимаю, о чем вы. Пожалуйста, переформулируйте вопрос.»\n Возможно только значение true. Если признак не применим, это свойство не включается в ответ.")
    private Boolean dangerousContext;
}
