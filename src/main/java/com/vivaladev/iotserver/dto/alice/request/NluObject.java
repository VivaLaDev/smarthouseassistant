package com.vivaladev.iotserver.dto.alice.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(description = "Слова и именованные сущности, которые Диалоги извлекли из запроса пользователя. Подробнее https://tech.yandex.ru/dialogs/alice/doc/nlu-docpage/")
public class NluObject {

    @ApiModelProperty(notes = "Массив слов из произнесенной пользователем фразы.")
    private List<String> tokens;

    @ApiModelProperty(notes = "Массив именованных сущностей.")
    private List<EntityObject> entities;
}
