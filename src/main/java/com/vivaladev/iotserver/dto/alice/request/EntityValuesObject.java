package com.vivaladev.iotserver.dto.alice.request;

import lombok.Data;

@Data
public class EntityValuesObject {

    private String first_name;
    private String patronymic_name;
    private String last_name;
    private String country;
    private String city;
    private String street;
    private String house_number;
    private String airport;
    private String year;
    private String year_is_relative;
    private String month;
    private String month_is_relative;
    private String day;
    private String day_is_relative;
    private String hour;
    private String hour_is_relative;
    private String minute;
    private String minute_is_relative;
}
