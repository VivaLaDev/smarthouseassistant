package com.vivaladev.iotserver.dto.alice.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@ApiModel(description = "Контейнер для объектов ответа Алисы")
public class ResponseObject {

    @NotBlank
    @JsonProperty(required = true)
    @ApiModelProperty(notes = "Текст ответа Алисы")
    private String text;

    @ApiModelProperty(notes = "Генерация речи Алисы")
    private String tts;

    @Valid
    @ApiModelProperty(notes = "Кнопки для пользователя")
    private List<ButtonObject> buttons;

    @NotNull
    @JsonProperty("end_session")
    @ApiModelProperty(notes = "Следует ли закончить сессию разговора")
    private Boolean endSession;
}
