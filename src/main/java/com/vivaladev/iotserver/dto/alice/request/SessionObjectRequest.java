package com.vivaladev.iotserver.dto.alice.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SessionObjectRequest {

    @JsonProperty("new")
    @ApiModelProperty(notes = "Признак новой сессии.")
    private Boolean newSession;

    @JsonProperty(value = "session_id")
    @ApiModelProperty(notes = "Id сессии, максимум 64 символов.")
    private String sessionId;

    @JsonProperty(value = "skill_id")
    @ApiModelProperty(notes = "Id навыка, открыть https://.../developer/skills/<идентификатор>/ чтобы узнать номер")
    private String skillId;

    @JsonProperty(value = "message_id")
    @ApiModelProperty(notes = "Id сообщения в сессии")
    private Integer messageId;

    @JsonProperty(value = "user_id")
    @ApiModelProperty(notes = "Id пользователя")
    private String userId;
}
