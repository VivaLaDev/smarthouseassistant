package com.vivaladev.iotserver.dto.alice.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.json.JSONObject;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(description = "Кнопки, которые следует показать пользователю. \nВсе указанные кнопки выводятся после основного ответа Алисы, описанного в свойствах response.text и response.card. \nКнопки можно использовать как релевантные ответу ссылки или подсказки для продолжения разговора.")
public class ButtonObject {

    @NotBlank
    @JsonProperty(required = true)
    @ApiModelProperty(notes = "Если для кнопки не указано свойство url, по нажатию текст кнопки будет отправлен навыку как реплика пользователя.")
    private String title;

    @ApiModelProperty(notes = "Произвольный JSON, который Яндекс.Диалоги должны отправить обработчику, если данная кнопка будет нажата. \nМаксимум 4096 байт.")
    private JSONObject payload;

    @ApiModelProperty(notes = "Если свойство url не указано, по нажатию кнопки навыку будет отправлен текст кнопки.")
    private String url;

    @ApiModelProperty(notes = "Признак того, что кнопку нужно убрать после следующей реплики пользователя.")
    private Boolean hide;
}
