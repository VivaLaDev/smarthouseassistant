package com.vivaladev.iotserver.dto.alice.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "Запрос от Алисы, после активации навыка")
public class AliceRequest {

    @ApiModelProperty(notes = "Информация об устройстве, с помощью которого пользователь разговаривает с Алисой.")
    private MetaObject meta;

    @ApiModelProperty("Данные, полученные от пользователя.")
    private RequestObject request;

    @ApiModelProperty("Данные о сессии.")
    private SessionObjectRequest session;

    @ApiModelProperty(notes = "Версия протокола, всегда 1.0")
    private String version;
}
