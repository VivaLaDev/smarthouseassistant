package com.vivaladev.iotserver.dto.alice.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@ApiModel(description = "Объект сессии пользователя")
public class SessionObjectResponse {

    @NotBlank
    @JsonProperty(value = "session_id", required = true)
    @ApiModelProperty(notes = "Id сессии")
    private String sessionId;

    @NotNull
    @JsonProperty(value = "message_id", required = true)
    @ApiModelProperty(notes = "Id сообщения в сессии")
    private Integer messageId;

    @NotNull
    @JsonProperty(value = "user_id", required = true)
    @ApiModelProperty(notes = "Id пользователя")
    private String userId;
}
