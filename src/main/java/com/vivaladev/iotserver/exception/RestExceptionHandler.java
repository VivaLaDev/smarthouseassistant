package com.vivaladev.iotserver.exception;

import com.vivaladev.iotserver.dto.alice.response.AliceResponse;
import com.vivaladev.iotserver.dto.util.ErrorDto;
import com.vivaladev.iotserver.exception.android.AndroidBadCredentionalException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ApiException.class)
    public ResponseEntity<AliceResponse> handlingApiException(ApiException ex) {
        AliceResponse response = DefaultAliceResponseProvider.buildSmartErrorResponse(ex.getErrorContainer().getMessage());
        log.error("Business error catch in restExceptionHandler: ", ex);
        return sendErrorResponse(response);
    }

    @ExceptionHandler(UnexpectedCommandException.class) //todo поменять ошибку на неожиданную комманду
    public ResponseEntity<AliceResponse> handlingUnexpectedComandException(ApiException ex) {
        AliceResponse response = DefaultAliceResponseProvider.buildSmartErrorResponse(ex.getErrorContainer().getMessage());
        log.error("Business error catch in restExceptionHandler: ", ex);
        return sendErrorResponse(response);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorDto> handlingCommonException(Exception ex) {
//        log.error("Unexpected exception catch in restExceptionHandler: ", ex);
//        return buildInternalError(); //todo uncommit for alice
        log.error("Internal error: ", ex);
        ErrorDto errorDto = ErrorDto.builder()
                .code(HttpStatus.INTERNAL_SERVER_ERROR)
                .message(ex.getMessage())
                .build();
        return new ResponseEntity<>(errorDto, HttpStatus.OK);
    }

    //Android exception block
    @ExceptionHandler(AndroidBadCredentionalException.class)
    public ResponseEntity<ErrorDto> handleBadAuthException(AndroidBadCredentionalException ex) {
        ErrorDto errorDto = ErrorDto.builder()
                .code(HttpStatus.INTERNAL_SERVER_ERROR)
                .message("Bad credential")
                .build();
        return new ResponseEntity<>(errorDto, HttpStatus.UNAUTHORIZED);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        log.error("Request validation error", ex);
        AliceResponse response = DefaultAliceResponseProvider.buildInternalErrorResponse();
        return super.handleExceptionInternal(ex, response, headers, HttpStatus.OK, request);
    }

    private ResponseEntity<AliceResponse> sendErrorResponse(AliceResponse aliceResponse) {
        log.error("Alice return error message to user: {}", aliceResponse.getResponse().getText());
        return new ResponseEntity<>(aliceResponse, HttpStatus.OK);
    }
}
