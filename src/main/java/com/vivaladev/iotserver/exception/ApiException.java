package com.vivaladev.iotserver.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiException extends RuntimeException {

    private String message;

    private ErrorContainer errorContainer;

    public ApiException(ErrorContainer errorContainer) {
        this.errorContainer = errorContainer;
    }
}
