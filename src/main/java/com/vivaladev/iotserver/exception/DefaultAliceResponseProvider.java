package com.vivaladev.iotserver.exception;

import com.vivaladev.iotserver.dto.alice.response.AliceResponse;
import com.vivaladev.iotserver.util.AliceConstant;

public class DefaultAliceResponseProvider {

    public static AliceResponse buildInternalErrorResponse() {
        AliceResponse response = new AliceResponse();
        response.setVersion(AliceConstant.PROTOCOL_VERSION);
        // TODO: 21.04.2019 implement / сказать что все пошло плохо, временно не стоит пытаться повторить запрос, надо связаться с поддержкой с дефолтной командой <ошибка {текст}> todo добавить обработчик в распознаватель
        return response;
    }

    public static AliceResponse buildSmartErrorResponse(String text) {
        AliceResponse response = new AliceResponse();
        response.setVersion(AliceConstant.PROTOCOL_VERSION);
        // TODO: 21.04.2019 implement / распознать тип ошибки и сказать примерный план решения / пока что просто что то пошло не так
        return response;
    }

    public static AliceResponse buildApprovedCommandResponse() {
        AliceResponse response = new AliceResponse();
        response.setVersion(AliceConstant.PROTOCOL_VERSION);
        // TODO: 24.04.2019 implement / сказать, что команда принята к исполнению
        return response;
    }

    public static AliceResponse buildUnauthorizeResponse() {
        AliceResponse response = new AliceResponse();
        response.setVersion(AliceConstant.PROTOCOL_VERSION);
        // TODO: 28.04.2019 implement / сказать, что необходима авторизация по userCode
        return response;
    }

    public static AliceResponse buildStartWorkingResponse() {
        AliceResponse response = new AliceResponse();
        response.setVersion(AliceConstant.PROTOCOL_VERSION);
        // TODO: 29.04.2019 implement / сказать, что можно начать работу, в системе авторизовано
        return response;
    }
}
