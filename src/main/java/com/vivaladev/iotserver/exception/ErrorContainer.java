package com.vivaladev.iotserver.exception;

import org.springframework.http.HttpStatus;

public enum ErrorContainer {
    BAD_REQUEST("Bad request", HttpStatus.BAD_REQUEST),
    AUTHORIZE_ERROR("Api not found", HttpStatus.UNAUTHORIZED),
    INTERNAL_ERROR("Internal server error", HttpStatus.INTERNAL_SERVER_ERROR),
    ;

    private String message;

    private HttpStatus httpStatus;

    ErrorContainer(String message, HttpStatus httpStatus) {
        this.message = message;
        this.httpStatus = httpStatus;
    }

    public String getMessage() {
        return message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

}
