package com.vivaladev.iotserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IotAssistantServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(IotAssistantServerApplication.class, args);
    }

}
