package com.vivaladev.iotserver.util;

import java.util.Arrays;

public enum EnteringType {
    VOICE("SimpleUtterance"),
    BUTTON("ButtonPressed");

    private String value;

    public String getValue() {
        return value;
    }

    EnteringType(String value) {
        this.value = value;
    }

    public CardTypeConstant of(String value) {
        return Arrays.stream(CardTypeConstant.values())
                .filter(cardTypeConstant -> value.equals(cardTypeConstant.getValue()))
                .findFirst()
                .orElse(null);
    }
}
