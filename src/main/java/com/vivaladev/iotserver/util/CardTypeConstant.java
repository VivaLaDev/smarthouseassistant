package com.vivaladev.iotserver.util;

import java.util.Arrays;

public enum CardTypeConstant {

    BIG_IMAGE("BigImage"),
    IMAGE_LIST("ItemsList");

    private String value;

    CardTypeConstant(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public CardTypeConstant of(String value) {
        return Arrays.stream(CardTypeConstant.values())
                .filter(cardTypeConstant -> value.equals(cardTypeConstant.getValue()))
                .findFirst()
                .orElse(null);
    }
}
