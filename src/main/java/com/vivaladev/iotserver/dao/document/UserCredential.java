package com.vivaladev.iotserver.dao.document;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document(collection = "userCredentials")
public class UserCredential {

    @Id
    private String userCode;

    private String email;

    private String hash;

    private String webhookKey;
}
