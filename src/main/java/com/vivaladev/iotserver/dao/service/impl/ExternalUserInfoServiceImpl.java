package com.vivaladev.iotserver.dao.service.impl;

import com.vivaladev.iotserver.dao.repository.ExternalUserInfoRepository;
import com.vivaladev.iotserver.dao.service.ExternalUserInfoService;
import com.vivaladev.iotserver.dao.service.RecognizeService;
import com.vivaladev.iotserver.dto.alice.request.AliceRequest;
import com.vivaladev.iotserver.entity.ExternalUserInfo;
import org.springframework.stereotype.Service;

@Service
public class ExternalUserInfoServiceImpl implements ExternalUserInfoService {

    private ExternalUserInfoRepository userInfoRepository;
    private RecognizeService recognizeService;

    public ExternalUserInfoServiceImpl(ExternalUserInfoRepository userInfoRepository,
                                       RecognizeService recognizeService) {
        this.userInfoRepository = userInfoRepository;
        this.recognizeService = recognizeService;
    }

    @Override
    public boolean checkExistingClient(String clientId) {
        return userInfoRepository.existsById(clientId);
    }

    @Override
    public ExternalUserInfo getUserInfoByClientId(String clientId) {
        return userInfoRepository.findById(clientId).orElse(null);
    }

    @Override
    public boolean addClientIdWithMappedUserCode(AliceRequest request) {
        String userCode = recognizeService.recognizeUserCode(request.getRequest().getCommand());
        String clientId = request.getSession().getUserId();

        ExternalUserInfo externalUserInfo = new ExternalUserInfo();
        externalUserInfo.setClientId(clientId);
        externalUserInfo.setUserCode(userCode);

        ExternalUserInfo save = userInfoRepository.save(externalUserInfo);
        return save != null;
    }
}
