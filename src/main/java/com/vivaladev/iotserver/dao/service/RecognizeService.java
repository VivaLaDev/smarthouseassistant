package com.vivaladev.iotserver.dao.service;

public interface RecognizeService {

    String recognizeSkill(String userMessage);

    String recognizeUserCode(String userMessage);
}
