package com.vivaladev.iotserver.dao.service.impl;

import com.vivaladev.iotserver.dao.service.RecognizeService;
import org.springframework.stereotype.Service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class RecognizeServiceImpl implements RecognizeService {

    private static final String SKILL_PATTERN = "\\s[a-zA-Zа-яА-Я\\s\\d]*$";
    private static final String USER_CODE_PATTERN = "[\\d]{8}";

    @Override
    public String recognizeSkill(String userMessage) {
        Pattern r = Pattern.compile(SKILL_PATTERN);
        Matcher m = r.matcher(userMessage);
        if (m.find()) {
            return m.group(0).trim();
        }
        throw new IllegalArgumentException("Unrecognized command text: " + userMessage);
    }

    @Override
    public String recognizeUserCode(String userMessage) {
        Pattern r = Pattern.compile(USER_CODE_PATTERN);
        Matcher m = r.matcher(userMessage);
        if (m.find()) {
            return m.group(0);
        }
        throw new IllegalArgumentException("Unrecognized command text: " + userMessage);
    }
}
