package com.vivaladev.iotserver.dao.service;

import com.vivaladev.iotserver.dto.alice.request.AliceRequest;
import com.vivaladev.iotserver.entity.ExternalUserInfo;

public interface ExternalUserInfoService {

    boolean checkExistingClient(String clientId);

    ExternalUserInfo getUserInfoByClientId(String clientId);

    boolean addClientIdWithMappedUserCode(AliceRequest request);
}
