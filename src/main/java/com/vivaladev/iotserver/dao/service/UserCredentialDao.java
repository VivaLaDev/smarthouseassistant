package com.vivaladev.iotserver.dao.service;

import com.vivaladev.iotserver.dao.document.UserCredential;
import com.vivaladev.iotserver.dto.android.WebhookChangeRequest;

public interface UserCredentialDao {

    UserCredential saveNewUser(UserCredential user);

    UserCredential getUserByCode(String userCode);

    void deleteUserByCode(String userCode);

    boolean checkUserExist(String userCode);

    boolean checkUserExist(String email, String password);

    UserCredential getUserByCred(String email, String password);

    void changeWebhook(WebhookChangeRequest request);
}
