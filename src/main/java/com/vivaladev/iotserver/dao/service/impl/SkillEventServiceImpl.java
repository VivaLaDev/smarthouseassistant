package com.vivaladev.iotserver.dao.service.impl;

import com.vivaladev.iotserver.dao.repository.UserCommandRecognizeRepository;
import com.vivaladev.iotserver.dao.service.SkillEventService;
import com.vivaladev.iotserver.dto.android.UserCommandMapping;
import com.vivaladev.iotserver.entity.UserCommandRecognize;
import com.vivaladev.iotserver.exception.UnexpectedCommandException;
import com.vivaladev.iotserver.exception.android.AndroidUnexpectedCommandException;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class SkillEventServiceImpl implements SkillEventService {

    private final UserCommandRecognizeRepository recognizeRepository;

    public SkillEventServiceImpl(UserCommandRecognizeRepository recognizeRepository) {
        this.recognizeRepository = recognizeRepository;
    }

    @Override
    public UserCommandRecognize getUserCommandsByUserCode(String userCode) {
        return recognizeRepository.findById(userCode).orElseThrow(UnexpectedCommandException::new);
    }

    @Override
    public boolean checkExistingCommand(String userCode) {
        return recognizeRepository.existsById(userCode);
    }

    @Override
    public UserCommandRecognize addMapping(UserCommandMapping commandMapping) {
        //1) дернуть из репозитория по userCode и добавить в готовую мапу
        //2) создать объект и положить, как щас
        String userCode = commandMapping.getUserCode();
        if (recognizeRepository.existsById(userCode)) {
            return saveNewMapping2ExistUserCode(userCode, commandMapping);
        }

        return saveNewMapping2NewUserCode(userCode, commandMapping);
    }

    @Override
    public void deleteMapping(UserCommandMapping userCommandMapping) {
        String userCode = userCommandMapping.getUserCode();
        if (recognizeRepository.existsById(userCode)) {
            Optional<UserCommandRecognize> byId = recognizeRepository.findById(userCode);
            recognizeRepository.save(byId
                    .map(userMapping -> {
                        userMapping.getSkill2EventMap().remove(userCommandMapping.getSkillName());
                        return userMapping;
                    })
                    .orElseThrow(AndroidUnexpectedCommandException::new));
            return;
        }
        throw new AndroidUnexpectedCommandException();
    }

    private UserCommandRecognize saveNewMapping2ExistUserCode(String userCode, UserCommandMapping commandMapping) {
        UserCommandRecognize userCommandRecognize = recognizeRepository.findById(userCode).get();
        Map<String, String> skill2EventMap = userCommandRecognize.getSkill2EventMap();
        skill2EventMap.put(commandMapping.getSkillName(), commandMapping.getEventName());
        return recognizeRepository.save(userCommandRecognize);
    }

    private UserCommandRecognize saveNewMapping2NewUserCode(String userCode, UserCommandMapping commandMapping) {
        UserCommandRecognize commandRecognize = new UserCommandRecognize();
        commandRecognize.setUserCode(userCode);
        commandRecognize.setSkill2EventMap(new HashMap<String, String>() {{
            put(commandMapping.getSkillName(), commandMapping.getEventName());
        }});
        return recognizeRepository.save(commandRecognize);
    }
}
