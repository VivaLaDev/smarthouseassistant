package com.vivaladev.iotserver.dao.service;

import com.vivaladev.iotserver.dto.android.UserCommandMapping;
import com.vivaladev.iotserver.entity.UserCommandRecognize;

public interface SkillEventService {

    UserCommandRecognize getUserCommandsByUserCode(String userCode);

    boolean checkExistingCommand(String userCode);

    UserCommandRecognize addMapping(UserCommandMapping commandMapping);

    void deleteMapping(UserCommandMapping userCommandMapping);
}
