package com.vivaladev.iotserver.dao.service.impl;

import com.vivaladev.iotserver.dao.document.UserCredential;
import com.vivaladev.iotserver.dao.repository.UserCredentialRepository;
import com.vivaladev.iotserver.dao.service.UserCredentialDao;
import com.vivaladev.iotserver.dto.android.WebhookChangeRequest;
import com.vivaladev.iotserver.exception.android.AndroidBadCredentionalException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class UserCredentialDaoImpl implements UserCredentialDao {

    private UserCredentialRepository credentialRepository;

    public UserCredentialDaoImpl(UserCredentialRepository credentialRepository) {
        this.credentialRepository = credentialRepository;
    }

    @Override
    public UserCredential saveNewUser(UserCredential user) {
        log.debug("Saving user: {}", user);
        return credentialRepository.save(user);
    }

    @Override
    public UserCredential getUserByCode(String userCode) {
        log.debug("getting user by code: {}", userCode);
        return credentialRepository.findById(userCode).orElse(null);
    }

    @Override
    public void deleteUserByCode(String userCode) {
        log.debug("Deleting user by code: {}", userCode);
        credentialRepository.deleteById(userCode);
    }

    @Override
    public boolean checkUserExist(String userCode) {
        log.debug("Check user existing by code: {}", userCode);
        return credentialRepository.existsById(userCode);
    }

    @Override
    public boolean checkUserExist(String email, String password) {
        return credentialRepository.existsByEmailAndHash(email, password);
    }

    @Override
    public UserCredential getUserByCred(String email, String password) {
        return credentialRepository.findByEmailAndHash(email, password);
    }

    @Override
    public void changeWebhook(WebhookChangeRequest request) {
        String userCode = request.getUserCode();
        credentialRepository.findById(userCode).map(cred -> {
            cred.setWebhookKey(request.getWebhookKey());
            return credentialRepository.save(cred);
        }).orElseThrow(AndroidBadCredentionalException::new);
    }
}
