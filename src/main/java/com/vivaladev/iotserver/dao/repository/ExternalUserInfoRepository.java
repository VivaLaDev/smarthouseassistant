package com.vivaladev.iotserver.dao.repository;

import com.vivaladev.iotserver.entity.ExternalUserInfo;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExternalUserInfoRepository extends MongoRepository<ExternalUserInfo, String> {
}
