package com.vivaladev.iotserver.dao.repository;

import com.vivaladev.iotserver.dao.document.UserCredential;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserCredentialRepository extends MongoRepository<UserCredential, String> {

    boolean existsByEmailAndHash(String email, String hash);

    UserCredential findByEmailAndHash(String email, String hash);
}
