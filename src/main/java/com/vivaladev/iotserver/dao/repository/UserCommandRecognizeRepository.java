package com.vivaladev.iotserver.dao.repository;

import com.vivaladev.iotserver.entity.UserCommandRecognize;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserCommandRecognizeRepository extends MongoRepository<UserCommandRecognize, String> {
}
