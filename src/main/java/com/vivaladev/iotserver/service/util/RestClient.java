package com.vivaladev.iotserver.service.util;

import java.util.Map;

public interface RestClient {

    <T> T sendGetRequest(String url, Map<String, String> params, Class<T> responseClass);

    <Response, Request> Response sendPostRequest(String url, Request requestEntity, Class<Response> responseClass);
}
