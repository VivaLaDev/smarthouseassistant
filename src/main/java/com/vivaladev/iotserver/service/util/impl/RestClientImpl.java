package com.vivaladev.iotserver.service.util.impl;

import com.vivaladev.iotserver.service.util.RestClient;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Service
public class RestClientImpl implements RestClient {

    private RestTemplate restTemplate;

    public RestClientImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public <T> T sendGetRequest(String url, Map<String, String> params, Class<T> responseClass) {
        return restTemplate.getForObject(url, responseClass, params);
    }

    @Override
    public <Response, Request> Response sendPostRequest(String url, Request requestEntity, Class<Response> responseClass) {
        return restTemplate.postForObject(url, requestEntity, responseClass);
    }
}
