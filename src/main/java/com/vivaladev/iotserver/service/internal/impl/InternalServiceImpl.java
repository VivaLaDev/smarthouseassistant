package com.vivaladev.iotserver.service.internal.impl;

import com.vivaladev.iotserver.dto.util.InfoDto;
import com.vivaladev.iotserver.service.internal.InternalService;
import com.vivaladev.iotserver.service.util.RestClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class InternalServiceImpl implements InternalService {

    private static final String SELF_REQUEST_URL = "/vivaladev/internal/health/check";
    private static final String HTTP_LOCALHOST = "http://localhost:";

    @Value("${server.port}")
    private String localPort;

    private RestClient restClient;

    public InternalServiceImpl(RestClient restClient) {
        this.restClient = restClient;
    }

    @Override
    public String selfRequestCheck() {
        String url = HTTP_LOCALHOST + localPort + SELF_REQUEST_URL;
        InfoDto infoDto = restClient.sendGetRequest(url, new HashMap<>(), InfoDto.class);
        return "response from this service - " + infoDto.getMessage();
    }
}
