package com.vivaladev.iotserver.service.internal;

public interface InternalService {

    String selfRequestCheck();
}
