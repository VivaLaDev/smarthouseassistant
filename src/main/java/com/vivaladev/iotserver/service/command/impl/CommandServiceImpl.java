package com.vivaladev.iotserver.service.command.impl;

import com.vivaladev.iotserver.dao.service.SkillEventService;
import com.vivaladev.iotserver.dto.android.CommandList;
import com.vivaladev.iotserver.dto.android.UserCommandMapping;
import com.vivaladev.iotserver.service.broker.MessageBroker;
import com.vivaladev.iotserver.service.command.CommandService;
import org.springframework.stereotype.Service;

@Service
public class CommandServiceImpl implements CommandService {

    private final MessageBroker messageBroker;
    private final SkillEventService skillEventService;

    public CommandServiceImpl(MessageBroker messageBroker, SkillEventService skillEventService) {
        this.messageBroker = messageBroker;
        this.skillEventService = skillEventService;
    }

    @Override
    public CommandList getCommandByUserCode(String userCode) {
        return messageBroker.getCommandsForClient(userCode);
    }

    @Override
    public void addNewSkill2EventMapping(UserCommandMapping commandMapping) {
        skillEventService.addMapping(commandMapping);
    }

    @Override
    public void deleteCommandMapping(UserCommandMapping userCommandMapping) {
        skillEventService.deleteMapping(userCommandMapping);
    }
}
