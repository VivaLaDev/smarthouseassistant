package com.vivaladev.iotserver.service.command;

import com.vivaladev.iotserver.dto.android.CommandList;
import com.vivaladev.iotserver.dto.android.UserCommandMapping;

public interface CommandService {

    CommandList getCommandByUserCode(String clientId);

    void addNewSkill2EventMapping(UserCommandMapping commandMapping);

    void deleteCommandMapping(UserCommandMapping userCommandMapping);
}
