package com.vivaladev.iotserver.service.broker.impl;

import com.vivaladev.iotserver.dto.android.CommandList;
import com.vivaladev.iotserver.service.broker.MessageBroker;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class MessageBrokerImpl implements MessageBroker {

    private final Map<String, List<String>> commandMap = new HashMap<>();

    @Override
    public synchronized void putCommand(String userCode, String command) {
        List<String> commands = commandMap.get(userCode);
        if (commands == null) {
            synchronized (commandMap) {
                commandMap.put(userCode, Collections.singletonList(command));
                return;
            }
        }
        List<String> userSkills = new ArrayList<>(commands);
        userSkills.add(command);
        synchronized (commandMap) {
            commandMap.put(userCode, userSkills);
        }
    }

    @Override
    public synchronized CommandList getCommandsForClient(String userCode) {
        List<String> list = null;
        synchronized (commandMap) {
            List<String> commands = commandMap.get(userCode);
            if (commands != null) {
                list = commands;
                commandMap.put(userCode, null);
            }
        }
        return new CommandList(Optional.ofNullable(list).orElse(null));
    }
}
