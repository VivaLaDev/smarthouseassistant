package com.vivaladev.iotserver.service.broker;

import com.vivaladev.iotserver.dto.android.CommandList;

public interface MessageBroker {

    void putCommand(String clientId, String command);

    CommandList getCommandsForClient(String clientId);
}
