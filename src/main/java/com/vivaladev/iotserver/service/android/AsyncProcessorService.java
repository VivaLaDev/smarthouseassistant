package com.vivaladev.iotserver.service.android;

import com.vivaladev.iotserver.dto.alice.request.AliceRequest;

public interface AsyncProcessorService {

    void process(AliceRequest request, String userCode);
}
