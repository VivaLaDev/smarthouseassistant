package com.vivaladev.iotserver.service.android.impl;

import com.vivaladev.iotserver.dao.service.RecognizeService;
import com.vivaladev.iotserver.dao.service.SkillEventService;
import com.vivaladev.iotserver.dto.alice.request.AliceRequest;
import com.vivaladev.iotserver.entity.UserCommandRecognize;
import com.vivaladev.iotserver.exception.UnexpectedCommandException;
import com.vivaladev.iotserver.service.android.AsyncProcessorService;
import com.vivaladev.iotserver.service.broker.MessageBroker;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AsyncProcessorServiceImpl implements AsyncProcessorService {

    private final MessageBroker messageBroker;
    private final SkillEventService skillEventService;
    private final RecognizeService recognizeService;

    public AsyncProcessorServiceImpl(MessageBroker messageBroker,
                                     SkillEventService skillEventService,
                                     RecognizeService recognizeService) {
        this.messageBroker = messageBroker;
        this.skillEventService = skillEventService;
        this.recognizeService = recognizeService;
    }

    @Override
    public void process(AliceRequest request, String userCode) {
        synchronized (skillEventService) {
            if (!skillEventService.checkExistingCommand(userCode)) {
                throw new UnexpectedCommandException();
            }
        }
        asyncCommandProcess(request, userCode);
    }

    @Async
    public void asyncCommandProcess(AliceRequest request, String userCode) {
        synchronized (skillEventService) {
            UserCommandRecognize userCommandsByUserCode = skillEventService.getUserCommandsByUserCode(userCode);
            String userCommand = Optional.ofNullable(request.getRequest().getCommand()).orElseThrow(IllegalArgumentException::new);

            String userSkill = recognizeService.recognizeSkill(userCommand);
            String eventName = getEventName(userSkill, userCommandsByUserCode);
            synchronized (messageBroker) {
                messageBroker.putCommand(userCode, eventName);
            }
        }
    }

    private String getEventName(String userSkill, UserCommandRecognize userCommandsByUserCode) {
        final String[] userEventName = new String[1];
        userCommandsByUserCode.getSkill2EventMap()
                .forEach((skillName, eventName) -> {
                    if (skillName.equals(userSkill)) {
                        userEventName[0] = eventName;
                    }
                });
        return userEventName[0];
    }
}
