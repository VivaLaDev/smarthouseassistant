package com.vivaladev.iotserver.service.user;

import com.vivaladev.iotserver.dto.android.UserAuthorisationResponse;
import com.vivaladev.iotserver.dto.android.UserCodeResponse;
import com.vivaladev.iotserver.dto.android.WebhookChangeRequest;

public interface UserService {

    UserCodeResponse registerNewUser(String email, String password, String webhookKey);

    UserAuthorisationResponse authorizeUser(String email, String password);

    void changeWebhook(WebhookChangeRequest request);
}
