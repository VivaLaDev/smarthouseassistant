package com.vivaladev.iotserver.service.user.impl;

import com.vivaladev.iotserver.dao.document.UserCredential;
import com.vivaladev.iotserver.dao.service.UserCredentialDao;
import com.vivaladev.iotserver.dto.android.UserAuthorisationResponse;
import com.vivaladev.iotserver.dto.android.UserCodeResponse;
import com.vivaladev.iotserver.dto.android.WebhookChangeRequest;
import com.vivaladev.iotserver.exception.android.AndroidBadCredentionalException;
import com.vivaladev.iotserver.service.user.UserService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private UserCredentialDao dao;

    public UserServiceImpl(UserCredentialDao dao) {
        this.dao = dao;
    }


    @Override
    public UserCodeResponse registerNewUser(String email, String password, String webhookKey) {
        if (dao.checkUserExist(email, password)) {
            UserCredential userByCred = dao.getUserByCred(email, password);
            return new UserCodeResponse(userByCred.getUserCode());
        }

        String userCode;
        do {
            userCode = generateCode();
        } while (dao.checkUserExist(userCode));

        UserCredential userCredential = new UserCredential();
        userCredential.setUserCode(userCode);
        userCredential.setEmail(email);
        userCredential.setHash(password);
        userCredential.setWebhookKey(webhookKey);
        dao.saveNewUser(userCredential);

        return new UserCodeResponse(userCode);
    }

    @Override
    public UserAuthorisationResponse authorizeUser(String email, String password) {
        UserCredential userByCred = dao.getUserByCred(email, password);
        return Optional.ofNullable(userByCred)
                .map(validUser -> new UserAuthorisationResponse(validUser.getUserCode(), validUser.getWebhookKey()))
                .orElseThrow(AndroidBadCredentionalException::new);
    }

    @Override
    public void changeWebhook(WebhookChangeRequest request) {
        dao.changeWebhook(request);
    }

    private String generateCode() {
        StringBuilder builder = new StringBuilder();
        List<Integer> collect = new Random()
                .ints(8, 0, 10)
                .boxed()
                .collect(Collectors.toList());
        collect.forEach(builder::append);
        return builder.toString();
    }
}
